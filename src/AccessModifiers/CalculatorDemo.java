package AccessModifiers;


class Calculator
{
	public static int add(int a , int b) 
	{
		return a+b;
	}
	public static int multiply(int a , int b)
	{
		return a*b;
	}

}

public class CalculatorDemo
{
	public static void main(String[] args)
	{
		int addResult = Calculator.add(10,20);
		
		int multiplyResult = Calculator.multiply(10,20);
		
		System.out.println("AddResult = "+addResult);
		
		System.out.println("MultiplyResult = "+multiplyResult);
		
	}
}
