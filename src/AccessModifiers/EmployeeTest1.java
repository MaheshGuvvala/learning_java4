package AccessModifiers;
// package Object;

class Employees // parameterized Constructor
{
	String name;  // --->Instance Variables
	int id;
	
	
    Employees(String name , int id)   // parameterized Constructor
	{
		this.name = name;
		this.id = id;
		
	}
	public void print() 
	{
		System.out.println("Name is : "+name+","+"Id is : "+id);
		
	}

}


public class EmployeeTest1
{
	public static void main(String[] args) 
	{
		Employees mahesh = new Employees("mahesh",100);   //--->parameterized Constructor
		mahesh.print();
		
		Employees kiran = new Employees("kiran",200);  
		kiran.print();
		
	}
}
