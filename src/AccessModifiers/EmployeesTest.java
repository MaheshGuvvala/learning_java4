package AccessModifiers;
// package Object;

class Employee1       // No-argument Constructor example
{
	String name;  // -->Instance Variables
	int id;
	
    Employee1()   // -->No-argument Constructor
	{
		name = "mahesh";
		id = 100;
		
	}
  
	public void print()
	{
		System.out.println("Name is : "+name+","+"Id is : "+id);
	}
	
}


public class EmployeesTest
{
	public static void main(String[] args) 
	{
		Employee1 mahesh = new Employee1();
		mahesh.print();
		
	//	Employee1 kiran = new Employee1();
	//	kiran.print();
	// multiple No-Arg constructors not possible	
	}
}
