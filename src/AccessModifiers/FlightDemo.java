package AccessModifiers;


class Flight
{
		static int seats = 160;

		public void bookTicket(boolean bookTicket) 
		{
			if (bookTicket == true) 
			{
				seats -= 1;
			}
		}

}

public class FlightDemo
{
	public static void main(String[] args)
	{
		System.out.println("Initial seating capacity : "+Flight.seats);
		Flight f = new Flight();
		f.bookTicket(true);
		f.bookTicket(true);
		f.bookTicket(true);
		System.out.println("No of seats present now : " + Flight.seats);
	}
}
