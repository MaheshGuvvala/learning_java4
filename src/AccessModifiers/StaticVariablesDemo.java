package AccessModifiers;



class StaticVariables{
	static int a = 10;
	int b = 20;

	public void print() {

		System.out.println("Value of a = " + a);
		System.out.println("Value of b = " + b);

	}
}

 public class StaticVariablesDemo{
	
	public static void main(String[] args) {
		
		System.out.println(StaticVariables.a); // 10
		
		
		StaticVariables staticVariables = new StaticVariables();
		staticVariables.b=20;
				
		System.out.println(staticVariables.b); // 20
		
		
		
		StaticVariables staticVariables1 = new StaticVariables();
		staticVariables1.b=30;
		
		System.out.println(staticVariables1.b); // 30
		System.out.println(staticVariables1.a); // 10
		
		System.out.println(StaticVariables.a); // 10
	}
}
