package AccessModifiers;

class Student1 
{
	int id;         // --->Instance Variables
	String name;
	 
	Student1(String name , int id)  //--->parameterized Constructor
	{
		this.name = name;
		this.id = id;
	}
	public void print()
	{
		System.out.println("Name is :"+name+","+"id is : "+id);
	
	}

}

public class Student
{
	public static void main(String[] args) 
	{
		Student1 mahesh = new Student1("mahesh",100);   //--->parameterized Constructor
		mahesh.print();
		
	}
}