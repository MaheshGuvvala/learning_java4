package Arrays;

public class ArraysDemo 
{
	public static void main(String[] args) 
	{
		
	// Array is group of fixed and ordered sequence of homogeneous elements.	
		
	//	int[] marks = new int[6];
	//	marks[0]=56;
	//	marks[1]=69;
	//	marks[2]=98;
	//	marks[3]=74;
	//	marks[4]=85;
	//	marks[5]=65;
		
	//	System.out.println(marks[2]);
		
		
		int[] marks = new int[] {56,69,98,74,85,65};
	//	System.out.println(marks[2]);
		
	//	System.out.println(marks.length);
		
		// iterative statements (for loop is best choice for iterating through arrays)
		
	
		for(int i=0;i<=marks.length;i++)
		{
			if(marks[i]%2==0)
			{
				System.out.println(marks[i]+"is an even");
			}
			else
			{
				System.out.println(marks[i]+"is an odd");
			}
			System.out.println(marks[i]);
		}
		
	}

}
