package ArraysAssessment;

import java.util.Scanner;

public class PNEOZ
{
	public static void main(String[] args)
	   {
	      int i,countP=0, countN=0, countZ=0,countE=0,countO=0;
	      
	      int[] arr = new int[20];
	      Scanner input = new Scanner(System.in);
	      
	      System.out.print("Enter 20 Numbers: ");
	      for(i=0; i<20; i++)
	      {
	    	  arr[i] = input.nextInt();
	      }
	      
	      for(i=0; i<20; i++)
	      {
	         if(arr[i]<0)
	         {
	        	 countN++;
	         }
	         else if(arr[i]>0)
	         {
	        	 countP++;
	         }
	         else
	         {
	        	 countZ++;
	         }
	      }
	      
	      for(i=0;i<20;i++)
	      {
	    	  if(arr[i]%2 == 0)
		    	 {
		    		countE++;
		    	 }
		    	 else
		    	 {
		    		 countO++;
		    	 }
	    		 
	      }
	      
	      System.out.println("Total Positive Numbers are  : " +countP);
	      System.out.println("Total Negative Numbers are : " +countN);
	      System.out.println("Total Zero's are : " +countZ);
	      System.out.println("Total Even Numbers are : "+countE);
	      System.out.println("Total Odd Numbers are : "+countO);
	   }
}