package Composition;

class Engine{
	
	public void startEngine() {
		System.out.println("Starting Engine...");
	}
	
	public void stopEngine() {
		System.out.println("Stopping Engine...");
	}
}


// Coupling -> dependency on other object
// Cohesion -> responsibility of the object/class

// there should be less coupling between the objects and more cohesion.


class Employee{
	
	// must do only operations related to my employee object
	
	// bank operations here - no
}

class Students{
	
	// attend the school daily
	
	// other activities - stictly no	
}

class A{
	B b;
}

class B{
	C c;
}

class C{
	D d;
}

class D{
	E e;
}

class E{
	
}



// 		Composition                      Aggregation
// College has departments, and departments have professors

// A 
// |
// B

class Car {

	String name = "Lambhorgini";
	String color = "Yellow";
	Engine e = new Engine();
	
	public void start() {
		System.out.println("ignition gets turned on..");
		System.out.println("..car checks are performed");
		e.startEngine();
	}
	
	public void stop() {
		System.out.println("checks are performed before stopping the engine");
		e.stopEngine();
		System.out.println("ignition turned off");
	}
}

// diff between inheritance and composition


// a car has-a engine
// a house has-a door
// laptop has-a keyboard

// has-a relationship
public class CompositionDemo {

	public static void main(String[] args) {
		
		Car c = new Car();
		c.start();

		c.stop();
	}
}