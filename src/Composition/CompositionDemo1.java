package Composition;

class Student
{
	public void walkToSchool() {
		System.out.println("Going to school daily");
	}
	public void returnToHome() {
		System.out.println("come back to home");
	}
}

class School
{
	String name = "Mahesh";
	int id = 123;
	Student s = new Student();
	
	public void mrg() {
		System.out.println("wakeup and get ready for going to school");
		s.walkToSchool();
	}
	public void evg() {
		s.returnToHome();
		System.out.println("return to home and take some food");

	}
		
}
public class CompositionDemo1 
{
	public static void main(String[] args) {
		
		School obj = new School();
		
		obj.mrg();
		obj.evg();
		
	}
}
