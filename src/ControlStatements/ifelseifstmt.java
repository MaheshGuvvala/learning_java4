package ControlStatements;

public class ifelseifstmt 
{
	public static void main(String[] args)
	{
		int coffees_ordered_in_last_week = 4;
		int discount = 0;
		if (coffees_ordered_in_last_week > 5)
		{
			discount = 10;
		} else if (coffees_ordered_in_last_week > 10) 
		{
			discount = 15;
		} else
		{
			discount = 5;
		}
		System.out.println("This customer is eligible for a " + discount + "% discount.");
		}

}
