package DataTypes;

public class DataTypesDemo 
{

	public static void main(String[] args) 
	{
		byte b = 100;
		
		short s = 32767;
		
		int i = 2147483647;
		
		float f = 10.000000f;
		
		double d = 20.0;
		
		char c = 'a';
		
		System.out.println(b);
		System.out.println(s);
		System.out.println(i);
		System.out.println(f);
		System.out.println(d);
		System.out.println(c);
	}
}