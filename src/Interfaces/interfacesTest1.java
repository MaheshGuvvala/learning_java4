package Interfaces;

// Interfaces contains 100% abstract methods.

// parent class
interface Vehicle{
	
	public static final int noOfTyres = 4;
	public static final String color = " Red";
	
	public abstract void accelerate();
	
	public abstract void brake();
	
}
// 1. Interfaces can't contain a constructor.
// 2. By default,interfaces contain only abstract methods but no concrete methods until(till Java V1.7)
// 3. Even if you don't specify a method is abstract ,by default the methods are always abstract.
// 4. I can specify variables inside my interface,by default all the variables are public,static and final



//  Vehicle (Interface)
//     |
//  Buggati(Implementation)


// child class
class Buggati implements Vehicle{
	
	public void accelerate() {
		System.out.println("accelarating at top speed of 600km/hr"+" "+noOfTyres+" "+"Tyres  "
				+ ""+" "+"with color :"+color);
	}
	
	public void brake() {
		System.out.println("Applying brakes to your Buggati");
	}
	
}

class RangeRover implements Vehicle{

	
	public void accelerate() {
		System.out.println("RangeRover drawing a speed of 300km/hr");
		
	}


	public void brake() {
		System.out.println("Applying brakes to your Range Rover");
		
	}
	
}


public class interfacesTest1{
	public static void main(String[] args) {
//		Vehicle v1 = new Vehicle(); // CE : can't instantiate interface
		
		Vehicle v1 = new Buggati();
		v1.accelerate();//accelarating at top speed of 600km/hr

		v1.brake();// Applying brakes to your Buggati
        
		
		Vehicle v2 = new RangeRover();
		v2.accelerate();// RangeRover drawing a speed of 300km/hr

		v2.brake();// Applying brakes to your Range Rover

	}
}