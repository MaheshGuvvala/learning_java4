package Interfaces;

interface Bikes
{
	public static final int noOfTyres = 2;
	public static final int model = 2022;
	
	public abstract void version();
	
	public abstract void speed();
	
	public abstract void color();
}

class pulser implements Bikes
{

	@Override
	public void version() {
		// TODO Auto-generated method stub
		System.out.println("latest version 6");
	}

	@Override
	public void speed() {
		// TODO Auto-generated method stub
		System.out.println("150km/h");
	}

	@Override
	public void color() {
		// TODO Auto-generated method stub
		System.out.println("Red and black combination");
	}
	
}

class R15 implements Bikes{

	@Override
	public void version() {
		// TODO Auto-generated method stub
		System.out.println("latest version 6");
	}

	@Override
	public void speed() {
		// TODO Auto-generated method stub
		System.out.println("200km/h");
	}

	@Override
	public void color() {
		// TODO Auto-generated method stub
		System.out.println("Green and Black combination");
	}
}

class RoyalEnfield implements Bikes{

	@Override
	public void version() {
		// TODO Auto-generated method stub
		System.out.println("Latest version 6");
	}

	@Override
	public void speed() {
		// TODO Auto-generated method stub
		System.out.println("250km/h");
	}

	@Override
	public void color() {
		// TODO Auto-generated method stub
		System.out.println("silver color");
	}
	
}

public class interfacesTest2 
{
	public static void main(String[] args)
	{
		Bikes b1 = new pulser();
		b1.version();// latest version 6
		b1.speed();// 150km/h
		b1.color();// Red and black combination
	
		Bikes b2 = new R15();
		b2.version();// latest version 6
		b2.speed();// 200km/h
		b2.color();// Green and Black combination
		
		Bikes b3 = new RoyalEnfield();
		b3.version();// Latest version 6
		b3.speed();//  250km/h
		b3.color();//  silver color	
		
	}

}
