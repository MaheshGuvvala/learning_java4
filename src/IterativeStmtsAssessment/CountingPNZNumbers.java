package IterativeStmtsAssessment;

import java.util.Scanner;

public class CountingPNZNumbers 
{
	 public static void main(String[] args)
	   {
	      int num, countP=0, countN=0, countZ=0;
	      Scanner input = new Scanner(System.in);
	      
	      System.out.print("Enter 5 Numbers: ");
	      for(int i=0; i<5; i++)
	      {
	         num = input.nextInt();
	         if(num<0)
	         {
	        	 countN++; 
	         }
	            
	         else if(num>0)
	         {
	        	 countP++;

	         }
	         else
	         {
	        	 countZ++;
	         }
	      }
	      
	      System.out.println("Total Positive Number: " +countP);
	      System.out.println("Total Negative Number: " +countN);
	      System.out.println("Total Zero: " +countZ);
	   }

}
