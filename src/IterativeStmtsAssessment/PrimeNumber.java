package IterativeStmtsAssessment;

import java.util.Scanner;

public class PrimeNumber 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		System.out.println("please enter the number");
		
		int n = input.nextInt();
		
		if(n>0)
		{
			if(n%2!=0)
			{
				System.out.println("prime Number");
			}
			else
			{
				System.out.println("Not Prime Number");
			}
		}
		
		
	}

}
