package IterativeStmtsAssessment;

import java.util.Scanner;

public class ReversingNumber 
{
	public static void main(String args[])

	{

	int n=0;

	int rev =0;

	System.out.println("Input your number and press enter: ");

	Scanner input = new Scanner(System.in);

	n = input.nextInt();

	while( n != 0 )
	{
		rev = rev * 10;

	    rev = rev + n%10;

	    n = n/10;

	}
	
	System.out.println("Reverse of input number is: "+rev);
	}

}

