package IterativeStmtsAssessment;

public class SumOfNaturalNumbers 
{
	public static void main(String[] args) 
	{

	       int num = 10, i = 1, sum = 0;

	       while(i <= num)
	       {
	           sum = sum + i;
	           i++;
	       }

	       System.out.println("Sum of first 10 natural numbers is: "+sum);
	    }

}

// public static void main(String[] args) {

   // int num = 10, count, total = 0;

   // for(count = 1; count <= num; count++){
     //   total = total + count;
    //}

    //System.out.println("Sum of first 10 natural numbers is: "+total);
 //}

//public static void main(String[] args) {

   // int num, count, total = 0;

    
   // System.out.println("Enter the value of n:");
    //Scanner is used for reading user input
   // Scanner scan = new Scanner(System.in);
    //nextInt() method reads integer entered by user
  //  num = scan.nextInt();
    //closing scanner after use
  //  scan.close();
   // for(count = 1; count <= num; count++){
     //   total = total + count;
    //}

    // System.out.println("Sum of first "+num+" natural numbers is: "+total);
 // }