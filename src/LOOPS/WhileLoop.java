package LOOPS;
// Iterative Statements
public class WhileLoop
{
	public static void main(String[] args) 
	{   
		int i = 0;
		
		while(i<3)  // Entry Control Loop -->while
		{
			System.out.println("Hii");
			i++;
		}
		
	}

}


// sysntax:
// while(condition_is_met)
// {
//    (code execute)
//  }
