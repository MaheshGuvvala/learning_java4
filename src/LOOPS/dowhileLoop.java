package LOOPS;

import java.util.Scanner;

// Iterative Statements
public class dowhileLoop 
{
	public static void main(String[] args) 
	{
		int number = 8;
		
		int guess = 0;
		
		Scanner input = new Scanner(System.in);
		
		do 
		{
			System.out.print("Enter a number between 1 and 10: ");
		    guess = input.nextInt();
		    
		} while (number != guess);
		
		System.out.println("You're correct!");
	}
}

// 1.difference  between while,dowhile and for loop?
// ANS :
// while loop is generally used when you want to execute certain stmts until a condition is satisfied.
// do-while loop is generally used when you want to execute stmts atleast once before checking a condition.
// for loop are generally used when you know the number of times taat the stmt should be executed.

//-->syntax:
//  do
//  {
//     (code execute)
//   }while(condition_is_met)