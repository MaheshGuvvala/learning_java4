package LOOPS;
//package LOOPS;

import java.util.Scanner;

public class forLoop 
{
	public static void main(String[] args) 
	{

		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter number : ");
		
		int n = input.nextInt();
		
        for(int i=1; i <= 10; i++)
        {
            System.out.println(n+" * "+i+" = "+n*i);
        }
		
	}

}


// -->syntax:

// for(initialization ; condition ; inc/dec operator);

