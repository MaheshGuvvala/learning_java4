package MethodOverloading;

//In method overloading, method signature must be different.
//In method overriding, method signature must be same to its parent class


class SumFinder
{
	public int sum(int a, int b) {
		int result = a+b;
		return result;
	}
	
	public double sum(double a,double b) {
		double result = a+b;
		return result;
	}
}

public class MethodOverLoadingDemo 
{
	public static void main(String[] args) {
		SumFinder finder = new SumFinder();
		
		int s1 = finder.sum(10,20);
		System.out.println(s1);
		
		double s2 = finder.sum(10,20.1);
		System.out.println(s2);
	}

}
