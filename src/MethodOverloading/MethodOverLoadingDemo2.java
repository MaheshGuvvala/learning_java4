package MethodOverloading;

class MO1
{
	
	public void m1(StringBuffer s) {
		System.out.println("StringBuffer method");
	}
	
	public void m1(Object s) {
		System.out.println("Object method");
	}
	
}


//	   Object
//	
//String 	StringBuffer

public class MethodOverLoadingDemo2 
{

	public static void main(String[] args) {
		
		
		MO1 mo = new MO1();
		
		mo.m1(new String("Hello")); // Object method
		
		mo.m1(new StringBuffer("Hello")); // StringBuffer method
		
//		all the classes are either directly or indirectly child classes of Object class
		
		 
	}
}




