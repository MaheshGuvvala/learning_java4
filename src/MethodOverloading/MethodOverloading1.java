package MethodOverloading;

class M
{
	void add()
	{
		int a =10,b=20;
		int c=a+b;
		System.out.println("Sum="+c);
	}
	void add(int x,int y)
	{
		int z=x+y;
		System.out.println("Sum="+z);
	}
	
}
public class MethodOverloading1
{
	public static void main(String[] args) {
		M obj = new M();
		obj.add(); // Sum=30
		obj.add(10,30); // Sum=40
		
	}

}
