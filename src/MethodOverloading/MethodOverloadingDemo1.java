package MethodOverloading;

class MethodOverloading{
	
	public void m1(float f) {
		System.out.println("float method");
	}
	
	public void m1(char c) {
		System.out.println("char method");
	}
	
	public void m1(long l) {
		System.out.println("long method");
	}
	
}


//byte-->short-->int-->long-->float-->double
//	  -->char

public class MethodOverloadingDemo1 
{

	public static void main(String[] args) 
	{
		
		MethodOverloading overloading = new MethodOverloading();
		
		overloading.m1(10); // long method
		
		overloading.m1(10.0f); // float method
		
		overloading.m1(10); // long method
		
	}
}