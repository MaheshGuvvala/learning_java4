package MethodOverloading;

class OP
{
	void add() {
		int a= 10,b=15;
		System.out.println("a+b="+(a+b));
	}
}
public class OperatorOverloading 
{
	public static void main(String[] args) {
		OP obj = new OP();
		obj.add(); // a+b=25
		
	}

}
