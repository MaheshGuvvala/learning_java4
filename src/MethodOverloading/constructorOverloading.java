package MethodOverloading;

class A
{
	A(){
		System.out.println("Constructor without any argument");
	}
	
	A(int a){
		System.out.println("Constructor with argument");
	}
}
public class constructorOverloading
{
	public static void main(String[] args) 
	{
		A obj = new A(); // Constructor without any argument

		A obj1 = new A(10);  // Constructor with argument

	}

}
