
package MethodOverriding;

//In method overloading, method signature must be different.
//In method overriding, method signature must be same to its parent class

class Parents{
	public void eat() {
		System.out.println("I eat, only veg");
	}
	
	public void eat(String item) {
		System.out.println("In my parent, I am eating : "+ item);
	}
}

class Children extends Parents{
	public void eat() {
		System.out.println("I will eat only Biryani");
	}
}



public class MethodOverRidingDemo {

	public static void main(String[] args) {
		Children c = new Children();
		c.eat();
		c.eat("Cake");
	}
}