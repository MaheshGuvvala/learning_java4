package MethodOverriding;

// covariant return types
class P {

	public Number m1() {
		return 10;
	}
	
}

class C extends P {
	
	public Number m1() {
		return 10;
	}
}

class C1 extends P{
	
	public Float m1() {
		return 100.0f;
	}
}

class C2 extends P{
	
//	public String m1() {
//		return "Hello";
//	}
}

//									Object
//			  							|
//			Number (abstract class)						String
//	|		|		|		|		|	
//Short  Integer   Long    Float   Double



// covariant return types

public class MethodOverRidingDemo1
{
	public static void main(String[] args) 
	{		
		
	}

}
