package MethodOverriding;

class parent
{
	void display() {
		System.out.println("PARENT METHOD");
	}
}
class child extends parent
{
	void display() {
//		super.display();
		System.out.println("CHILD METHOD");
	}
}
public class MethodOverridingDemo2 
{
	public static void main(String[] args)
	{
		
		child c = new child();
		c.display();// CHILD METHOD
		
//		parent p = new child();
//		p.display();  // CHILD METHOD
	}

}
