package OOPS;

class ZooPark
{
	public void Tirupati() {
		System.out.println("There are different types of animals");
	}
}

class Lion extends ZooPark
{
	public void  eat() {
		System.out.println("Eating...");
	}
}

class Cheetha extends ZooPark
{
	public void run() {
		System.out.println("Running...");
	}
}

class HierarchicalInheritanceDemo
{
	public static void main(String[] args) 
	{
		Cheetha c = new Cheetha();
		
		c.run(); // Running...
		
//		c.eat(); // CT Error
		
		c.Tirupati(); // There are different types of animals
		
	}
}