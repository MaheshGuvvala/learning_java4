package OOPS;

class Parent1
{
	public void method1() 
	{
		System.out.println("method1 in parent class is executing.");
	}
	
}

class Children1 extends Parent1
{
	public void method1() 
	{
		System.out.println("method1 in child class is executing.");
	}

	public void method2() 
	{
		System.out.println("method2 in child class is executing.");
	}

}

public class InheritanceDemo1
{
	
	public static void main(String[] args)
	{

     	Parent1 p1 = new Parent1();
     	p1.method1(); //line no-7
//    	p1.method2();
	
     	Children1 c = new Children1();
     	c.method1();// line no-16
     	c.method2();// line no-21
     	
     	
 //   	Parent1 p = new Children1();
 //		p.method1(); // method1 in child class is executing // Run time check
 //    	p.method2(); // compile time checking
     
		
		
	}
	
}
