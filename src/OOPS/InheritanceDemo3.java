package OOPS;

class college
{
	public void clgname(){
		System.out.println("SISTK");
	}
	public void clgadd() {
		System.out.println("puttur");
	}
	public void course() {
		System.out.println("Btech");
	}
	public void stname() {
		System.out.println("mahesh");
	}
	
}

class student extends college{
	public void stname() {
		System.out.println("mahesh");
	}
	public void rollno() {
		System.out.println("123");
	}
	public void room() {
		System.out.println("101");
	}
}

class teacher extends student
{

}
public class InheritanceDemo3 
{
	public static void main(String[] args) 
	{
		
		college c = new student();
		c.stname();
		c.clgadd();
		c.clgname();
		c.course();
//		s.room();// Invalid
		
		college c1 = new college();
		c1.stname();
		c1.clgadd();
		c1.clgname();
		c1.course();
//		c1.rollno(); // invalid
		
		student  s = new student();
		s.stname();
		s.rollno();
		s.room();
		
		teacher t = new teacher();
		t.clgname();
		t.clgadd();
		t.rollno();
		t.room();
		
		
		
	}

}
