package OOPS;

class Animal1
{
	void eat() {
		System.out.println("Eating...");
	}
}

class Dog extends Animal1
{
	void bark() {
		System.out.println("Barking...");
	}
}

class BabyDog extends Dog 
{
	void weep() {
		System.out.println("Weeping...");
	}
}
public class MultiLevelInheritanceDemo 
{
	public static void main(String[] args) 
	{
		BabyDog b = new BabyDog();
		
		b.eat(); // Eating...
		
		b.bark(); // Barking...
		
		b.weep(); // Weeping...
		
	}

}
