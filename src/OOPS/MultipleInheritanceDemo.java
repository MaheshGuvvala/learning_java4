//package OOPS;
//
//class A1
//{
//	void msg() {
//		System.out.println("Hello");
//	}
//}
//
//class B1
//{
//	void msg() {
//		System.out.println("Welcome");
//	}
//}
//
//class C1 extends A1,B1   // Multiple Inheritance is not supported in JAVA because of ambiguity
//{
//	
//	
//}
//public class MultipleInheritanceDemo 
//{
//	public static void main(String[] args)
//	{
//		C1 obj = new C1();
//		obj.msg();
//	}
//
//}
//
