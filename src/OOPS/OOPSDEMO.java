package OOPS;

public class OOPSDEMO 
{
//	C (a sequential programming)


//	Int main()
//	{

	// do some operations or statements

//	return 0;
//	}

//	C++ or Java (object oriented programming)

	// every object is a real world entity

//	new Employee(name, age, salary, weight)=>object,
//	new Student(name, age, school, class)=>object,
//	new Car(color, length, accelerate(), brake() =>object,
//	Animal (type, walk(), eat())


class Tree
{

		private String name;
		private int heightOfTree;

		public void supplyOxygen()
		{
			// do some operations
		}
}

//	new Tree(�Tamarind�,  100 );

class Cat 
{

		private String eyesType;
		private String breed;
		private String name;

		public void eat(){}

		public void sleep(){}
	}


//	new Cat(�blue�, �normal�, �puppy�);

//	Javascript  (functional programming) => Java8

	// methods as an arguments to another function.






}
