package OOPS;

class Animal
{
	void eat() {
		System.out.println("Eating....");
	}
}

class Tiger extends Animal
{
	void bark() {
		System.out.println("Barking....");
	}
}

public class SingleInheritanceDemo 
{
	public static void main(String[] args) 
	{
		Tiger t = new Tiger();
		
		t.eat();// Eating
		
		t.bark();// Barking
		
	}

}
