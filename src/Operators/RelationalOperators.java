package Operators;

import java.util.Scanner;

public class RelationalOperators   // find the minimum number
{
	public static void main(String[] args) 
	{
		System.out.println("This is a program to find the minimum number");
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the first number");
		int firstNumber = input.nextInt();
		System.out.println("Please enter the second number");
		int secondNumber = input.nextInt();
		System.out.println("The Minimum number is : " + (firstNumber < secondNumber));

	}

}
