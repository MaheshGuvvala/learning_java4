package Polymorphism;

class P{
	public void m1() {
		System.out.println("Parents's m1 method...");
	}
	public void m1(int i) {
		System.out.println("Parents arg method...");
	}
}

class C1 extends P{
	public void m1() {
		System.out.println("Child1's m1 method...");
	}
}

class C2 extends P{
	public void m1() {
		System.out.println("Child2's m1 method...");
	}
}
public class CompileTimePolymorphismDemo {
	public static void main(String[] args) {
		
		P p = new P();
		p.m1(); // line 5 Parents's m1 method...
		
		P p1 = new C1();
		p1.m1(); // Line 14  Child1's m1 method...
		
		P p2 = new C2();
		p2.m1();  // Line 17  Child2's m1 method...

		
		p.m1(10); // Line 8   Parents arg method...

	}

}

//                            polymorphism

// Compile Time polymorphism                Run Time polymorphism

// Method Resolution happens during         Method execution happens during 
// Compile Time phase						Run Time Phase
// (Method overloading)                     (Method overriding)