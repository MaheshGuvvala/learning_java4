package Polymorphism;

class A{
	public void m1() {
		System.out.println("Parents's m1 method...");
	}
	public void m1(int i) {
		System.out.println("Parents arg method...");
	}
}

class B extends A{
	public void m1() {
		System.out.println("Child1's m1 method...");
	}
}

class C extends A{
	public void m1() {
		System.out.println("Child2's m1 method...");
	}
}

public class RunTimePolymorphismDemo {
	public static void main(String[] args) {
		A a = new A();
//		a.m1();
//		a.m1(10);
		execute(a);//Parents's m1 method...
		
		A a1 = new B();
		execute(a1);// Child1's m1 method...
	
		A a2 = new C();
		execute(a2); // Child2's m1 method...

	}
	
	public static void execute(A a1) {
		a1.m1();
	}

}
