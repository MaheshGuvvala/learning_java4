package Strings;

public class ImmutabilityDemo 
{
	public static void main(String[] args) 
	{

	// All strings are immutable in java 
	//	String s = "Java";
		
	//	String uppercase = s.toUpperCase();
		
	//	System.out.println(s); //Java
		
	//	System.out.println(uppercase);  //JAVA
		
		String s = "Java  ";
		
		String trimmed = s.trim();
		
		System.out.println(s);
		System.out.println(trimmed);
		
	}

}

// Immutability Design pattern(unchangeability):
//--> Once an object is created you cannot make changes to the existing
//object.But if you want to do some changes on the existing object,then 
//with those changes a new object will be created for you.

// -->All string objects are immutable whether they belong to SCP or
// heap area.