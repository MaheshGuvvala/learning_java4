package Strings;

public class StringBufferDemo 
{
	public static void main(String[] args) 
	{
		// Strings => Immutables
	    // StringBuffer and StringBuilder => Mutable
				
				
//				String s = "Learning Java is ";
//				String s1 = s.concat("easy");
//				System.out.println(s);
//				System.out.println(s1); //Learning Java is easy
				
//				Learning Java is 1
//				Learning Java is easy 2
				
				StringBuffer buffer = new StringBuffer("Learning Java is ");
				buffer.append("easy");
				// contains synchronized modifier , and thread safety , performance is slower
				
				StringBuilder builder = new StringBuilder("Learning Java is ");
				builder.append("easy");
				// no thread-safety=> no synchrnoized modfier, performance is faster
				
//				String
//				no thread safety => no synchronized modifier, performance is faster and immutability pattern exists
				
				System.out.println(buffer.toString());
				
	}
	
	
}
