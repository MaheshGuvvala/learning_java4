package Strings;

public class heap
{
	public static void main(String[] args) {

//		String str1 = "Hello";

//		Employee e = new Employee()

		String str2 = new String("Hello");  // stored in heap

		String str3 = new String("Hello");  // stored in heap

		
// == => reference comparison
		if (str2 == str3) {
			System.out.println("String objects are equal by reference");
		} else {
			System.out.println("String objects are not equal by reference");
		}
		
	
// .equal() -->object (content comparison)	
		
		if(str2.equals(str3)) {
			System.out.println("String objects are equal by content");
		}else {
			System.out.println("String objects are not equal by content");
		}
	}

}
