package Strings;

public class scp
{
	public static void main(String[] args) {

		String str1 = "Java"; // stored in String Constant Pool

		String str2 = "Java";  // stored in scp

		String str3 = "java";  // stored in scp
		
		String str4 =new String( "Java");  // stored in heap
		
//      ==  (reference comparison)

		if (str1 == str4) { 
			System.out.println("String literals and objects are equal by reference"); // this is executed
		} else {
			System.out.println("String literals and objects are not equal by reference");
		}
		
//      ==  (reference comparison)

		if (str1 == str2) { 
			System.out.println("String literals are equal by reference"); // this is executed
		} else {
			System.out.println("String literals are not equal by reference");
		}
		
//		.equals()   (content comparison)
		if(str1.equals(str2)) {
			System.out.println("str1 and str2 are equal by content");
		}else {
			System.out.println("str1 and str2 are not equal by content");
		}
		
//		.equalsIgnoreCase()
		
		if(str1.equalsIgnoreCase(str3)) {
			System.out.println("str1 and str3 are equal by content ignoring the case");
		}else {
			System.out.println("str1 and str3 are not equal by content ignoring the case");
		}
		
		

	}

}
