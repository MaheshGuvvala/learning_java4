package StringsAssesment;

public class CountVCDW 
{

	  public static void main(String[] args) 
	  {
	    String s = "Hello, have a good day";
	    
	    int vowels = 0, consonants = 0, digits = 0, spaces = 0;

	    s = s.toLowerCase();
	    
	    for (int i = 0; i < s.length(); ++i) 
	    {
	      char ch = s.charAt(i);

	      if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
	      {
	        ++vowels;
	      }

	      else if ((ch >= 'a' && ch <= 'z'))
	      {
	        ++consonants;
	      }
	      
	      else if (ch >= '0' && ch <= '9')
	      {
	        ++digits;
	      }
	      
	      else if (ch == ' ') 
	      {
	        ++spaces;
	      }
	    }

	    System.out.println("Number of Vowels : " + vowels);
	    System.out.println("Number of Consonants : " + consonants);
	    System.out.println("Number of Digits : " + digits);
	    System.out.println("Number of White spaces : " + spaces);
	  }

}
