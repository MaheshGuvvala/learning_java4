package abstractclasses;

abstract class Vehicle {
	
	int noOfTyres=4;
	
	public abstract void accelerate();

	public abstract void brake();
	
	public void baseEngine() {
		System.out.println("Implementing engine");
	}
}


class Car extends Vehicle{
	
	public void accelerate() {
		System.out.println("Accelerating car");
	}

	public void brake() {
		System.out.println("Braking car");
	}
}

class Lorry extends Vehicle{
	
	public void accelerate() {
		System.out.println("Accelerating lorry");
	}
	
	public void brake() {
		System.out.println("Braking Lorry");
	}
	
}

public class AbstractClassDemo {
	
	public static void main(String[] args) {
		
		Vehicle v1 = new Car();
		v1.accelerate(); // Accelerating car
		v1.brake();   // Braking car
		v1.baseEngine(); // Implementing engine		
	}
}

// 1. No public static final for variables like in interfaces.
// 2. Abstract classes can contain a constructor, the purpose of the constructor is not to initialize the object, but just to pass the values to it's parent class.
// 3. If a class contains one abstract method, then by default the class must be abstract class.
// 4. If a class is abstract, then it may or may not contain abstract methods.