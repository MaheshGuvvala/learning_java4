package abstractclasses;


// contain both abstract methods and concrete methods(implementation)

// interface P{
//  public void m1();
//}
// In interfaces all the methods are abstract methods

abstract class A{
	
	public abstract void m1(); // abstract method
	
	public void m2() {  // concrete method
		System.out.println("m2 method implemented in abstract  class");
	}
}

class B extends A{ // A is an abstract class

	public void m1() {
		System.out.println("m1 method implementation in child class");
	}
	
}




public class abstractTest1 {
	public static void main(String[] args) {
//  A a = new A();	// creating an abstract object not valid
		
	A a = new B();
	a.m1(); // m1 method implementation in child class

	a.m2(); //  m2 method implemented in abstract  class

		
		
		
	}

}
