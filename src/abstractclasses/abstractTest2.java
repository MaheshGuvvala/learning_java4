package abstractclasses;

abstract class C
{
	int noOfTyres = 2;
	
	public void version() {
		System.out.println("Latest model");
	}
	public abstract void color();
	
	public abstract void speed();
}

class D extends C
{

	@Override
	public void color() {
		// TODO Auto-generated method stub
		System.out.println("Red");
		
	}

	@Override
	public void speed() {
		// TODO Auto-generated method stub
		System.out.println("200km/h");
	}
	
}
public class abstractTest2 
{
	public static void main(String[] args) 
	{
		C obj = new D();
		
		obj.version();  // Latest model
		
		obj.color();  // Red
		
		obj.speed();  // 200km/h
		
	}

}
