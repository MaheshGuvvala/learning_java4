package abstractclasses;

abstract class Person{
	
	private String name;
	private int age;
	
	Person(String name, int age){
		this.name=name;
		this.age=age;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public abstract void printEmployee();
	
}

// Employee is-a person
class Employee extends Person{
	
	private int salary;
	private String designation;
	
	Employee(String name, int age, int salary, String designation){
		super(name, age);
		this.salary = salary;
		this.designation = designation;
	}
	
	public void printEmployee() {
		System.out.println("Employee with name : " + getName() + " and age  : " + getAge() + " with salary : "
				+ this.salary + " of designation : " + this.designation);
	}
	
}


public class abstractTest3 {
	
	public static void main(String[] args) {
		Person p = new Employee("Venkateshwara", 18, 23000, "Senior Software Engineer");
		p.printEmployee();
	}

}